package util

import org.gradle.api.Project
import java.util.*

@Suppress("UnstableApiUsage")
fun Project.cmd(vararg command: String) = try {
  providers.exec {
    commandLine(*command)
  }.standardOutput.asText.get().trim()
} catch (e: Throwable) {
  e.printStackTrace()
  null
}

fun Project.properties(fileName: String): Properties? {
  val file = file(fileName)
  if (!file.exists())
    return null
  val props = Properties()
  props.load(file.inputStream())
  return props
}
