/*
 * Copyright 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.chaosdorf.meteroid.theme.icons.filled

import androidx.compose.material.icons.materialPath
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import de.chaosdorf.meteroid.theme.icons.MeteroidIcons

val MeteroidIcons.Filled.WaterFull: ImageVector
  get() {
    if (_waterFull != null) {
      return _waterFull!!
    }
    _waterFull = ImageVector.Builder(
      name = "Filled.WaterFull",
      defaultWidth = 24f.dp,
      defaultHeight = 24f.dp,
      viewportWidth = 960f,
      viewportHeight = 960f
    ).apply {
      materialPath {
        moveTo(211f, 357f)
        quadTo(262f, 325f, 322.5f, 308f)
        quadTo(383f, 291f, 444f, 290f)
        quadTo(474f, 290f, 503.5f, 294f)
        quadTo(533f, 298f, 560f, 306f)
        quadTo(611.13f, 320f, 638.23f, 325f)
        quadTo(665.32f, 330f, 696f, 330f)
        lineTo(752f, 330f)
        lineTo(773f, 140f)
        lineTo(187f, 140f)
        lineTo(211f, 357f)
        close()

        moveTo(262f, 880f)
        quadTo(238.75f, 880f, 221.5f, 865.07f)
        quadTo(204.25f, 850.14f, 202f, 827f)
        lineTo(120f, 80f)
        lineTo(840f, 80f)
        lineTo(758f, 827f)
        quadTo(755.75f, 850.14f, 738.5f, 865.07f)
        quadTo(721.25f, 880f, 698f, 880f)
        lineTo(262f, 880f)
        close()

        moveTo(444f, 820f)
        quadTo(470f, 820f, 494.5f, 820f)
        quadTo(519f, 820f, 544f, 820f)
        quadTo(598f, 820f, 629f, 820f)
        quadTo(660f, 820f, 697f, 820f)
        lineTo(697f, 820f)
        lineTo(697f, 820f)
        quadTo(697f, 820f, 697f, 820f)
        quadTo(697f, 820f, 697f, 820f)
        lineTo(262f, 820f)
        quadTo(262f, 820f, 262f, 820f)
        quadTo(262f, 820f, 262f, 820f)
        lineTo(262f, 820f)
        quadTo(277.67f, 820f, 330.83f, 820f)
        quadTo(384f, 820f, 444f, 820f)
        close()
      }
    }.build()
    return _waterFull!!
  }

private var _waterFull: ImageVector? = null
