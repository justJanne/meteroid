/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.theme

import androidx.compose.material3.ColorScheme
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.compositeOver

val md_theme_light_primary = Color(0xFF345CA8)
val md_theme_light_onPrimary = Color(0xFFFFFFFF)
val md_theme_light_primaryContainer = Color(0xFFD9E2FF)
val md_theme_light_onPrimaryContainer = Color(0xFF001A43)
val md_theme_light_secondary = Color(0xFF8B5000)
val md_theme_light_onSecondary = Color(0xFFFFFFFF)
val md_theme_light_secondaryContainer = Color(0xFFFFDCBE)
val md_theme_light_onSecondaryContainer = Color(0xFF2C1600)
val md_theme_light_tertiary = Color(0xFFC0000A)
val md_theme_light_onTertiary = Color(0xFFFFFFFF)
val md_theme_light_tertiaryContainer = Color(0xFFFFDAD5)
val md_theme_light_onTertiaryContainer = Color(0xFF410001)
val md_theme_light_error = Color(0xFFC0000A)
val md_theme_light_errorContainer = Color(0xFFFFDAD5)
val md_theme_light_onError = Color(0xFFFFFFFF)
val md_theme_light_onErrorContainer = Color(0xFF410001)
val md_theme_light_background = Color(0xFFFEFBFF)
val md_theme_light_onBackground = Color(0xFF1B1B1F)
val md_theme_light_surface = Color(0xFFFEFBFF)
val md_theme_light_onSurface = Color(0xFF1B1B1F)
val md_theme_light_surfaceVariant = Color(0xFFE1E2EC)
val md_theme_light_onSurfaceVariant = Color(0xFF44474F)
val md_theme_light_outline = Color(0xFF757780)
val md_theme_light_inverseOnSurface = Color(0xFFF2F0F4)
val md_theme_light_inverseSurface = Color(0xFF303034)
val md_theme_light_inversePrimary = Color(0xFFAFC6FF)
val md_theme_light_shadow = Color(0xFF000000)
val md_theme_light_surfaceTint = Color(0xFF345CA8)
val md_theme_light_outlineVariant = Color(0xFFC5C6D0)
val md_theme_light_scrim = Color(0xFF000000)

val md_theme_dark_primary = Color(0xFFAFC6FF)
val md_theme_dark_onPrimary = Color(0xFF002D6C)
val md_theme_dark_primaryContainer = Color(0xFF15448F)
val md_theme_dark_onPrimaryContainer = Color(0xFFD9E2FF)
val md_theme_dark_secondary = Color(0xFFFFB870)
val md_theme_dark_onSecondary = Color(0xFF4A2800)
val md_theme_dark_secondaryContainer = Color(0xFF693C00)
val md_theme_dark_onSecondaryContainer = Color(0xFFFFDCBE)
val md_theme_dark_tertiary = Color(0xFFFFB4AA)
val md_theme_dark_onTertiary = Color(0xFF690003)
val md_theme_dark_tertiaryContainer = Color(0xFF930006)
val md_theme_dark_onTertiaryContainer = Color(0xFFFFDAD5)
val md_theme_dark_error = Color(0xFFFFB4AA)
val md_theme_dark_errorContainer = Color(0xFF930006)
val md_theme_dark_onError = Color(0xFF690003)
val md_theme_dark_onErrorContainer = Color(0xFFFFDAD5)
val md_theme_dark_background = Color(0xFF1B1B1F)
val md_theme_dark_onBackground = Color(0xFFE3E2E6)
val md_theme_dark_surface = Color(0xFF1B1B1F)
val md_theme_dark_onSurface = Color(0xFFE3E2E6)
val md_theme_dark_surfaceVariant = Color(0xFF44474F)
val md_theme_dark_onSurfaceVariant = Color(0xFFC5C6D0)
val md_theme_dark_outline = Color(0xFF8F9099)
val md_theme_dark_inverseOnSurface = Color(0xFF1B1B1F)
val md_theme_dark_inverseSurface = Color(0xFFE3E2E6)
val md_theme_dark_inversePrimary = Color(0xFF345CA8)
val md_theme_dark_shadow = Color(0xFF000000)
val md_theme_dark_surfaceTint = Color(0xFFAFC6FF)
val md_theme_dark_outlineVariant = Color(0xFF44474F)
val md_theme_dark_scrim = Color(0xFF000000)


val ColorScheme.secondaryGradient
  get() = ThemeGradient(
    listOf(
      secondaryContainer.copy(0.2f).compositeOver(surface),
      secondaryContainer,
    )
  )

val ColorScheme.onPrimaryContainerTinted
  get() = primary.copy(alpha = 0.4f).compositeOver(onPrimaryContainer)
