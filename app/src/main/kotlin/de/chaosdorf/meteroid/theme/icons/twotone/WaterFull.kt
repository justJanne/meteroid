/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.theme.icons.twotone

import androidx.compose.material.icons.materialPath
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import de.chaosdorf.meteroid.theme.icons.MeteroidIcons

val MeteroidIcons.TwoTone.WaterFull: ImageVector
  get() {
    if (_waterFull != null) {
      return _waterFull!!
    }
    _waterFull = ImageVector.Builder(
      name = "TwoTone.WaterFull",
      defaultWidth = 24f.dp,
      defaultHeight = 24f.dp,
      viewportWidth = 960f,
      viewportHeight = 960f
    ).apply {
      materialPath {
        moveTo(444f, 350f)
        quadTo(384f, 350f, 325f, 369.5f)
        quadTo(266f, 389f, 218f, 424f)
        lineTo(262f, 820f)
        quadTo(262f, 820f, 262f, 820f)
        quadTo(262f, 820f, 262f, 820f)
        lineTo(698f, 820f)
        quadTo(698f, 820f, 698f, 820f)
        quadTo(698f, 820f, 698f, 820f)
        lineTo(745f, 390f)
        lineTo(697f, 390f)
        quadTo(659.75f, 390f, 628.88f, 384.5f)
        quadTo(598f, 379f, 544f, 364f)
        quadTo(519f, 357f, 494.5f, 353.5f)
        quadTo(470f, 350f, 444f, 350f)
        close()

        moveTo(211f, 357f)
        quadTo(262f, 325f, 322.5f, 308f)
        quadTo(383f, 291f, 444f, 290f)
        quadTo(474f, 290f, 503.5f, 294f)
        quadTo(533f, 298f, 560f, 306f)
        quadTo(611.13f, 320f, 638.23f, 325f)
        quadTo(665.32f, 330f, 696f, 330f)
        lineTo(752f, 330f)
        lineTo(773f, 140f)
        lineTo(187f, 140f)
        lineTo(211f, 357f)
        close()

        moveTo(262f, 880f)
        quadTo(238.75f, 880f, 221.5f, 865.07f)
        quadTo(204.25f, 850.14f, 202f, 827f)
        lineTo(120f, 80f)
        lineTo(840f, 80f)
        lineTo(758f, 827f)
        quadTo(755.75f, 850.14f, 738.5f, 865.07f)
        quadTo(721.25f, 880f, 698f, 880f)
        lineTo(262f, 880f)
        close()

        moveTo(444f, 820f)
        quadTo(470f, 820f, 494.5f, 820f)
        quadTo(519f, 820f, 544f, 820f)
        quadTo(598f, 820f, 629f, 820f)
        quadTo(660f, 820f, 697f, 820f)
        lineTo(697f, 820f)
        lineTo(697f, 820f)
        quadTo(697f, 820f, 697f, 820f)
        quadTo(697f, 820f, 697f, 820f)
        lineTo(262f, 820f)
        quadTo(262f, 820f, 262f, 820f)
        quadTo(262f, 820f, 262f, 820f)
        lineTo(262f, 820f)
        quadTo(277.67f, 820f, 330.83f, 820f)
        quadTo(384f, 820f, 444f, 820f)
        close()
      }

      materialPath(fillAlpha = 0.3f, strokeAlpha = 0.3f) {
        moveTo(444f, 350f)
        quadTo(384f, 350f, 325f, 369.5f)
        quadTo(266f, 389f, 218f, 424f)
        lineTo(262f, 820f)
        quadTo(262f, 820f, 262f, 820f)
        quadTo(262f, 820f, 262f, 820f)
        lineTo(698f, 820f)
        quadTo(698f, 820f, 698f, 820f)
        quadTo(698f, 820f, 698f, 820f)
        lineTo(745f, 390f)
        lineTo(697f, 390f)
        quadTo(659.75f, 390f, 628.88f, 384.5f)
        quadTo(598f, 379f, 544f, 364f)
        quadTo(519f, 357f, 494.5f, 353.5f)
        quadTo(470f, 350f, 444f, 350f)
        close()
      }
    }.build()
    return _waterFull!!
  }

private var _waterFull: ImageVector? = null
