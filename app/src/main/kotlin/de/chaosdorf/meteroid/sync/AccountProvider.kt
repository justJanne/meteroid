/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.sync

import android.util.Log
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.model.*
import javax.inject.Inject

class AccountProvider @Inject constructor(
  private val serverRepository: ServerRepository,
  private val userRepository: UserRepository,
  private val pinnedUserRepository: PinnedUserRepository,
) {
  suspend fun account(serverId: ServerId, userId: UserId): AccountInfo? {
    val server = serverRepository.get(serverId)
    val user = userRepository.get(serverId, userId)
    val pinned = pinnedUserRepository.isPinned(serverId, userId)

    return if (server == null || user == null) null
    else AccountInfo(server, user, pinned)
  }

  suspend fun togglePin(serverId: ServerId, userId: UserId) {
    if (pinnedUserRepository.isPinned(serverId, userId)) {
      Log.e("DEBUG", "Unpinning $serverId, $userId")
      pinnedUserRepository.delete(serverId, userId)
    } else {
      Log.e("DEBUG", "Pinning $serverId, $userId")
      pinnedUserRepository.save(PinnedUser(serverId, userId))
    }
  }
}
