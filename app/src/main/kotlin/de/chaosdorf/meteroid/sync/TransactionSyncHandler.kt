/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.sync

import androidx.room.withTransaction
import de.chaosdorf.mete.model.MeteApiFactory
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.TransactionId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.MeteroidDatabase
import de.chaosdorf.meteroid.model.Server
import de.chaosdorf.meteroid.model.Transaction
import de.chaosdorf.meteroid.model.TransactionRepository
import de.chaosdorf.meteroid.sync.base.BaseIncrementalSyncHandler
import kotlinx.datetime.*
import javax.inject.Inject

class TransactionSyncHandler @Inject constructor(
  private val factory: MeteApiFactory,
  private val db: MeteroidDatabase,
  private val repository: TransactionRepository
) : BaseIncrementalSyncHandler<Pair<Server, UserId>, Transaction, TransactionSyncHandler.Key>() {
  data class Key(
    val server: ServerId, val transaction: TransactionId
  )

  override suspend fun <T> withTransaction(block: suspend () -> T): T =
    db.withTransaction(block)

  override suspend fun store(entry: Transaction) =
    repository.save(entry)

  override suspend fun delete(key: Key) =
    repository.delete(key.server, key.transaction)

  override fun entryToKey(entry: Transaction) = Key(entry.serverId, entry.transactionId)

  override suspend fun loadStored(context: Pair<Server, UserId>): List<Transaction> =
    repository.getAll(context.first.serverId, context.second)

  override suspend fun loadCurrent(context: Pair<Server, UserId>): List<Transaction> {
    val (server, userId) = context
    val api = factory.newInstance(server.url)
    val startDate = LocalDate(1970, 1, 1)
    val endDate = Clock.System.now().toLocalDateTime(TimeZone.UTC).date
      .plus(1, DateTimeUnit.DAY)

    val loadedEntries = api.listTransactions(
      userId,
      startDate.year, startDate.month.value, startDate.dayOfMonth,
      endDate.year, endDate.month.value, endDate.dayOfMonth,
    ).entries
    return loadedEntries.map { Transaction.fromModel(server, userId, it) }
  }

  override suspend fun loadLatestEntry(context: Pair<Server, UserId>): Transaction? =
    repository.getLatest(context.first.serverId, context.second)

  override suspend fun loadAdded(
    context: Pair<Server, UserId>,
    latest: Transaction
  ): List<Transaction> {
    val (server, userId) = context
    val api = factory.newInstance(server.url)
    val startDate = latest.timestamp.toLocalDateTime(TimeZone.UTC).date
    val endDate = Clock.System.now().toLocalDateTime(TimeZone.UTC).date
      .plus(1, DateTimeUnit.DAY)

    val loadedEntries = api.listTransactions(
      userId,
      startDate.year, startDate.month.value, startDate.dayOfMonth,
      endDate.year, endDate.month.value, endDate.dayOfMonth,
    ).entries
    return loadedEntries.map { Transaction.fromModel(server, userId, it) }
  }
}
