/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.sync

import android.util.Log
import de.chaosdorf.mete.model.MeteApiFactory
import de.chaosdorf.meteroid.model.*
import de.chaosdorf.meteroid.sync.base.SyncHandler
import de.chaosdorf.meteroid.util.newServer
import kotlinx.coroutines.flow.combine
import java.math.BigDecimal
import javax.inject.Inject

class SyncManager @Inject constructor(
  private val factory: MeteApiFactory,
  private val serverRepository: ServerRepository,
  private val userSyncHandler: UserSyncHandler,
  private val drinkSyncHandler: DrinkSyncHandler,
  private val transactionSyncHandler: TransactionSyncHandler
) {
  val syncState = combine(
    userSyncHandler.state,
    drinkSyncHandler.state,
    transactionSyncHandler.state
  ) { states ->
    states.find { it is SyncHandler.State.Error }
      ?: states.find { it == SyncHandler.State.Loading }
      ?: SyncHandler.State.Idle
  }

  suspend fun checkOffline(server: Server): Boolean {
    val updated = factory.newServer(server.serverId, server.url)
    return if (updated == null) {
      true
    } else {
      serverRepository.save(updated)
      false
    }
  }

  suspend fun sync(server: Server, user: User?, incremental: Boolean) {
    try {
      userSyncHandler.sync(server)
      drinkSyncHandler.sync(server)
      if (user != null) {
        Log.i(
          "SyncManager",
          "syncing transactions for user ${user.userId}, incremental ${incremental}"
        )
        if (incremental) {
          transactionSyncHandler.syncIncremental(Pair(server, user.userId))
        } else {
          transactionSyncHandler.sync(Pair(server, user.userId))
        }
      }
    } catch (e: Exception) {
      Log.e(
        "SyncManager",
        "Could not finish transaction for ${user?.name} (${user?.userId}) on ${server.url}",
        e
      )
    }
  }

  suspend fun purchase(account: AccountInfo, drink: Drink, count: Int) {
    val api = factory.newInstance(account.server.url)
    try {
      Log.i(
        "SyncManager",
        "Syncing purchase of ${drink.drinkId}/${drink.drinkId} for ${account.user.name}/${account.user.userId}"
      )
      for (i in 0 until count) {
        api.purchase(account.user.userId, drink.drinkId)
      }
      sync(account.server, account.user, incremental = true)
    } catch (e: Exception) {
      Log.e(
        "Sync",
        "Could not finish transaction for ${account.user.name} (${account.user.userId}) on ${account.server.url}",
        e
      )
    }
  }

  suspend fun deposit(account: AccountInfo, amount: BigDecimal) {
    try {
      val api = factory.newInstance(account.server.url)
      api.deposit(account.user.userId, amount)
      sync(account.server, account.user, incremental = true)
    } catch (e: Exception) {
      Log.e(
        "Sync",
        "Could not finish transaction for ${account.user.name} (${account.user.userId}) on ${account.server.url}",
        e
      )
    }
  }

  suspend fun withdraw(account: AccountInfo, amount: BigDecimal) {
    val api = factory.newInstance(account.server.url)
    try {
      api.withdraw(account.user.userId, amount)
      sync(account.server, account.user, incremental = true)
    } catch (e: Exception) {
      Log.e(
        "Sync",
        "Could not finish transaction for ${account.user.name} (${account.user.userId}) on ${account.server.url}",
        e
      )
    }
  }
}
