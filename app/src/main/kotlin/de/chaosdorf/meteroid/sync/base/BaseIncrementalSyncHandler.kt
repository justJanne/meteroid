/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.sync.base

import android.util.Log

abstract class BaseIncrementalSyncHandler<Context, Entry, Key> :
  BaseSyncHandler<Context, Entry, Key>(),
  IncrementalSyncHandler<Context> {
  abstract suspend fun loadLatestEntry(context: Context): Entry?
  abstract suspend fun loadAdded(context: Context, latest: Entry): List<Entry>

  override suspend fun sync(context: Context) {
    super.sync(context)
  }

  override suspend fun syncIncremental(context: Context) {
    if (syncState.compareAndSet(SyncHandler.State.Idle, SyncHandler.State.Loading) ||
      syncState.compareAndSet(SyncHandler.State.Error(), SyncHandler.State.Loading)
    ) {
      Log.w(this::class.simpleName, "Started incremental sync")
      try {
        withTransaction {
          val latestEntry = loadLatestEntry(context)
          if (latestEntry != null) {
            val addedEntries = loadAdded(context, latestEntry)
            for (loadedEntry in addedEntries) {
              store(loadedEntry)
            }
          } else {
            // If we can't do an incremental sync, do a full sync
            val loadedEntries = loadCurrent(context)
            val storedEntries = loadStored(context)
            val storedKeys = storedEntries.map(::entryToKey).toSet()
            val loadedKeys = loadedEntries.map(::entryToKey).toSet()
            val removedKeys = storedKeys - loadedKeys
            for (removedKey in removedKeys) {
              Log.e("SyncHandler", "deleting: $removedKey")
              delete(removedKey)
            }
            for (loadedEntry in loadedEntries) {
              store(loadedEntry)
            }
          }
        }
        syncState.value = SyncHandler.State.Idle
        Log.w(this::class.simpleName, "Finished incremental sync")
      } catch (e: Exception) {
        Log.e(this::class.simpleName, "Error while syncing data", e)
        syncState.value = SyncHandler.State.Error("Error while syncing data: $e")
      }
    } else {
      Log.w(this::class.simpleName, "Already syncing, disregarding sync request")
    }
  }
}
