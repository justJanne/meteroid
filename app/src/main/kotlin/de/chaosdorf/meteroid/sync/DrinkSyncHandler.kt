/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.sync

import androidx.room.withTransaction
import de.chaosdorf.mete.model.DrinkId
import de.chaosdorf.mete.model.MeteApiFactory
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.meteroid.MeteroidDatabase
import de.chaosdorf.meteroid.model.Drink
import de.chaosdorf.meteroid.model.DrinkRepository
import de.chaosdorf.meteroid.model.Server
import de.chaosdorf.meteroid.sync.base.BaseSyncHandler
import javax.inject.Inject

class DrinkSyncHandler @Inject constructor(
  private val factory: MeteApiFactory,
  private val db: MeteroidDatabase,
  private val repository: DrinkRepository
) : BaseSyncHandler<Server, Drink, DrinkSyncHandler.Key>() {
  data class Key(
    val server: ServerId, val drink: DrinkId
  )

  override suspend fun <T> withTransaction(block: suspend () -> T): T =
    db.withTransaction(block)

  override suspend fun store(entry: Drink) =
    repository.save(entry)

  override suspend fun delete(key: Key) =
    repository.delete(key.server, key.drink)

  override fun entryToKey(entry: Drink) = Key(entry.serverId, entry.drinkId)

  override suspend fun loadStored(context: Server): List<Drink> =
    repository.getAll(context.serverId)

  override suspend fun loadCurrent(context: Server): List<Drink> {
    val api = factory.newInstance(context.url)
    val loadedEntries = api.listDrinks()
    return loadedEntries.map { Drink.fromModel(context, it) }
  }
}
