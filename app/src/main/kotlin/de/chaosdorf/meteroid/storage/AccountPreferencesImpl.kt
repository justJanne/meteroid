/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.storage

import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.longPreferencesKey
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapLatest
import javax.inject.Inject

class AccountPreferencesImpl @Inject constructor(
  private val dataStore: DataStore<Preferences>
) : AccountPreferences {
  override val state: Flow<AccountPreferences.State> =
    dataStore.data.mapLatest {
      val serverId = it[SERVER_KEY] ?: -1L
      val userId = it[USER_KEY] ?: -1L
      Log.i("AccountPreferences", "Restored account data: $userId@$serverId")

      AccountPreferences.State(
        if (serverId >= 0) ServerId(serverId) else null,
        if (userId >= 0) UserId(userId) else null,
      )
    }

  override suspend fun setServer(server: ServerId?) {
    Log.i("AccountPreferences", "Setting account to -1@${server?.value ?: -1L}")
    dataStore.edit {
      it[SERVER_KEY] = server?.value ?: -1L
      it[USER_KEY] = -1L
    }
  }

  override suspend fun setUser(server: ServerId, user: UserId?) {
    Log.i("AccountPreferences", "Setting account to ${user?.value ?: -1L}@${server.value}")
    dataStore.edit {
      it[SERVER_KEY] = server.value
      it[USER_KEY] = user?.value ?: -1L
    }
  }

  private companion object {
    val SERVER_KEY = longPreferencesKey("serverId")
    val USER_KEY = longPreferencesKey("userId")
  }
}
