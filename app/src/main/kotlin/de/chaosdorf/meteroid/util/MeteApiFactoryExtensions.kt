/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.util

import de.chaosdorf.mete.model.MeteApiFactory
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.meteroid.model.Server
import java.net.URI

suspend fun MeteApiFactory.newServer(serverId: ServerId, baseUrl: String): Server? = try {
  val api = newInstance(baseUrl)
  val manifest = api.getManifest()

  val icon = manifest?.icons?.maxByOrNull {
    it.sizes?.split("x")
      ?.mapNotNull(String::toIntOrNull)
      ?.reduce(Int::times)
      ?: 0
  }

  val iconUrl = icon?.src?.let { URI(baseUrl).resolve(it).toString() }

  Server(
    serverId,
    manifest?.name,
    baseUrl,
    iconUrl
  )
} catch (_: Exception) {
  null
}
