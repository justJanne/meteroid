/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.util

import kotlinx.coroutines.suspendCancellableCoroutine
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException


suspend fun Call.await() = suspendCancellableCoroutine { continuation ->
  continuation.invokeOnCancellation {
    cancel()
  }
  enqueue(object : Callback {
    override fun onFailure(call: Call, e: IOException) {
      continuation.resumeWithException(e)
    }

    override fun onResponse(call: Call, response: Response) {
      if (response.isSuccessful) {
        val body = response.body
        if (body == null) {
          continuation.resumeWithException(KotlinNullPointerException("Response from ${call.request().method}.${call.request().url} was null but response body type was declared as non-null"))
        } else {
          continuation.resume(body)
        }
      } else {
        continuation.resumeWithException(IOException("Response from ${call.request().method}.${call.request().url} returned a non-successful response code: ${response.code}"))
      }
    }
  })
}
