/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import de.chaosdorf.mete.model.MeteApiFactory
import de.chaosdorf.mete.v1.MeteApiV1Factory
import de.chaosdorf.meteroid.MeteroidDatabase
import de.chaosdorf.meteroid.model.*
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
  @Singleton
  @Provides
  fun provideDatabase(
    @ApplicationContext context: Context
  ): MeteroidDatabase = Room
    .databaseBuilder(context, MeteroidDatabase::class.java, "mete")
    .build()

  @Provides
  fun provideDrinkRepository(
    database: MeteroidDatabase
  ): DrinkRepository = database.drinks()

  @Provides
  fun provideUserRepository(
    database: MeteroidDatabase
  ): UserRepository = database.users()

  @Provides
  fun providePinnedUserRepository(
    database: MeteroidDatabase
  ): PinnedUserRepository = database.pinnedUsers()

  @Provides
  fun provideTransactionRepository(
    database: MeteroidDatabase
  ): TransactionRepository = database.transactions()

  @Provides
  fun provideServerRepository(
    database: MeteroidDatabase
  ): ServerRepository = database.server()

  @Provides
  fun providesMeteFactory(): MeteApiFactory = MeteApiV1Factory
}
