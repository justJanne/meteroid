/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.navigation

import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.storage.AccountPreferences
import de.chaosdorf.meteroid.sync.base.SyncHandler
import de.chaosdorf.meteroid.ui.MeteroidScreen
import de.chaosdorf.meteroid.util.findStartDestination

@Composable
fun MeteroidNavigation(navController: NavController, viewModel: NavigationViewModel) {
  val backStackEntry by navController.currentBackStackEntryAsState()
  val activeRoute = MeteroidScreen.byRoute(backStackEntry?.destination?.route)

  val expanded by viewModel.expanded.collectAsState()
  val account by viewModel.account.collectAsState()
  val entries by viewModel.entries.collectAsState()
  val syncState by viewModel.syncState.collectAsState()

  LaunchedEffect(navController) {
    navController.addOnDestinationChangedListener { _, _, arguments ->
      val serverId = arguments?.getLong("server")?.let(::ServerId)
      val userId = arguments?.getLong("user")?.let(::UserId)

      viewModel.account.value = AccountPreferences.State(serverId, userId)
      viewModel.expanded.value = false
    }
  }

  BackHandler(expanded) {
    viewModel.expanded.value = false
  }

  val verticalContentPadding: Dp by animateDpAsState(if (expanded) 4.dp else 0.dp, label = "verticalContentPadding")
  val shadowElevation: Dp by animateDpAsState(if (expanded) 16.dp else 4.dp, label = "shadowElevation")

  Surface(
    Modifier.fillMaxWidth().animateContentSize()
      .padding(start = 16.dp, end = 16.dp, top = 16.dp, bottom = 128.dp),
    shape = RoundedCornerShape(32.dp),
    shadowElevation = shadowElevation,
    tonalElevation = 8.dp
  ) {
    Box {
      LazyColumn(contentPadding = PaddingValues(vertical = verticalContentPadding)) {
        items(entries, key = NavigationElement::key) { entry ->
          val isCurrent = entry.isCurrent(activeRoute, account?.server, account?.user)

          NavigationAnimationContainer(
            expanded || isCurrent
          ) {
            when (entry) {
              is NavigationElement.ServerElement -> NavigationServerItem(expanded, entry.server) {
                viewModel.expanded.value = true
              }

              is NavigationElement.UserElement -> NavigationUserItem(expanded,
                entry.user,
                entry.pinned,
                viewModel::togglePin,
                onExpand = { viewModel.expanded.value = true }
              ) {
                if (isCurrent) {
                  viewModel.expanded.value = false
                } else {
                  navController.navigate(
                    MeteroidScreen.Home.Purchase.build(
                      entry.user.serverId, entry.user.userId
                    )
                  ) {
                    launchSingleTop = true
                    restoreState = false
                    popUpTo(findStartDestination(navController.graph).id) {
                      saveState = false
                    }
                  }
                  viewModel.selectUser(entry.user.serverId, entry.user.userId)
                }
              }

              is NavigationElement.UserListElement -> NavigationUserListItem {
                if (NavigationElement.ServerElement(entry.server)
                    .isCurrent(activeRoute, account?.server, account?.user)
                ) {
                  viewModel.expanded.value = false
                } else {
                  navController.navigate(MeteroidScreen.UserList.build(entry.server.serverId)) {
                    launchSingleTop = true
                    restoreState = false
                    popUpTo(findStartDestination(navController.graph).id) {
                      saveState = false
                    }
                  }
                }
              }

              NavigationElement.AddServerElement -> NavigationAddServerItem(expanded,
                onExpand = { viewModel.expanded.value = true }
              ) {
                if (isCurrent) {
                  viewModel.expanded.value = false
                } else {
                  navController.navigate(MeteroidScreen.AddServer.build())
                }
              }

              NavigationElement.SettingsElement -> NavigationSettingsItem(
                expanded,
                onExpand = { viewModel.expanded.value = true }
              ) {
                if (isCurrent) {
                  viewModel.expanded.value = false
                } else {
                  navController.navigate(MeteroidScreen.Settings.build())
                }
              }
            }
          }
        }
      }
      AnimatedVisibility(syncState == SyncHandler.State.Loading, enter = fadeIn(), exit = fadeOut()) {
        LinearProgressIndicator(Modifier.align(Alignment.TopCenter).requiredHeight(2.dp).fillMaxWidth())
      }
    }
  }
}
