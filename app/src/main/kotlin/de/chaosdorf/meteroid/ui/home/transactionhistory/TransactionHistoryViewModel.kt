/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home.transactionhistory

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.model.DrinkRepository
import de.chaosdorf.meteroid.model.TransactionRepository
import de.chaosdorf.meteroid.sync.AccountProvider
import de.chaosdorf.meteroid.sync.SyncManager
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.math.BigDecimal
import javax.inject.Inject
import kotlin.time.Duration.Companion.minutes

@HiltViewModel
class TransactionHistoryViewModel @Inject constructor(
  savedStateHandle: SavedStateHandle,
  private val accountProvider: AccountProvider,
  private val syncManager: SyncManager,
  repository: TransactionRepository,
  drinkRepository: DrinkRepository
) : ViewModel() {
  private val serverId = ServerId(checkNotNull(savedStateHandle["server"]))
  private val userId = UserId(checkNotNull(savedStateHandle["user"]))

  val transactions: StateFlow<List<TransactionInfo>> = combine(
    repository.getAllFlow(serverId, userId), drinkRepository.getAllFlow(serverId)
  ) { transactions, drinks ->
    transactions.map { transaction ->
      TransactionInfo(transaction,
        drinks.firstOrNull { drink -> drink.drinkId == transaction.drinkId })
    }
  }.mapLatest { list ->
    list.mergeAdjecentDeposits().filter {
      it.drink != null || it.transaction.difference.abs() >= 0.01.toBigDecimal()
    }
  }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())

  fun sync() {
    viewModelScope.launch {
      accountProvider.account(serverId, userId)?.let { account ->
        syncManager.sync(account.server, account.user, incremental = true)
      }
    }
  }
}

fun List<TransactionInfo>.mergeAdjecentDeposits(): List<TransactionInfo> {
  val result = mutableListOf<TransactionInfo>()
  for (entry in this) {
    val previous = result.lastOrNull()
    if (previous != null && previous.transaction.difference > BigDecimal.ZERO && entry.transaction.difference > BigDecimal.ZERO && previous.drink == null && entry.drink == null && entry.transaction.timestamp.minus(
        previous.transaction.timestamp
      ) < 5.minutes
    ) {
      result.removeLast()
      result.add(
        entry.copy(
          transaction = entry.transaction.copy(
            difference = entry.transaction.difference + previous.transaction.difference
          )
        )
      )
    } else {
      result.add(entry)
    }
  }
  return result
}
