/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home.purchase

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.model.Drink
import de.chaosdorf.meteroid.model.DrinkRepository
import de.chaosdorf.meteroid.sync.AccountProvider
import de.chaosdorf.meteroid.sync.SyncManager
import de.chaosdorf.meteroid.util.update
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PurchaseViewModel @Inject constructor(
  private val savedStateHandle: SavedStateHandle,
  private val accountProvider: AccountProvider,
  private val syncManager: SyncManager,
  drinkRepository: DrinkRepository,
) : ViewModel() {
  val serverId = ServerId(checkNotNull(savedStateHandle["server"]))
  val userId = UserId(checkNotNull(savedStateHandle["user"]))

  val filters: StateFlow<Set<Filter>> =
    savedStateHandle.getStateFlow("filters", setOf(Filter.Active))

  val drinks: StateFlow<List<Drink>> = combine(
    drinkRepository.getAllFlow(serverId),
    filters
  ) { drinks, filters ->
    drinks.filter { item ->
      filters.all { filter -> filter.matches(item) }
    }
  }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())

  fun toggleFilter(filter: Filter) {
    savedStateHandle.update<Set<Filter>>("filters", emptySet()) { filters ->
      if (filters.contains(filter)) filters - filter
      else filters + filter
    }
  }

  fun purchase(item: Drink, count: Int, onBack: () -> Unit) {
    viewModelScope.launch {
      accountProvider.account(serverId, userId)?.let { account ->
        syncManager.purchase(account, item, count)
        if (!account.pinned) {
          onBack()
        }
      }
    }
  }

  fun sync() {
    viewModelScope.launch {
      accountProvider.account(serverId, userId)?.let { account ->
        syncManager.sync(account.server, account.user, incremental = true)
      }
    }
  }

  enum class Filter {
    CaffeineFree,
    Active;

    fun matches(item: Drink) = when (this) {
      CaffeineFree -> item.caffeine == 0
      Active -> item.active
    }
  }
}
