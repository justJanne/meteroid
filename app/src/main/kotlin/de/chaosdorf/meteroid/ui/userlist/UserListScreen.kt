/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.userlist

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import de.chaosdorf.meteroid.ui.MeteroidScreen
import de.chaosdorf.meteroid.util.findStartDestination

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun UserListScreen(
  navController: NavController,
  viewModel: UserListViewModel,
  contentPadding: PaddingValues = PaddingValues(),
) {
  val users by viewModel.users.collectAsState()
  val pinnedUsers by viewModel.pinnedUsers.collectAsState()

  LazyColumn(contentPadding = contentPadding) {
    if (pinnedUsers.isNotEmpty()) {
      stickyHeader("pinned") {
        ListItem(headlineContent = {
          Text(
            "Pinned",
            style = MaterialTheme.typography.labelMedium,
            modifier = Modifier.padding(start = 8.dp, end = 8.dp, top = 16.dp, bottom = 4.dp)
          )
        })
      }

      items(
        pinnedUsers,
        key = { "pinned-${it.serverId}-${it.userId}" },
      ) { user ->
        UserListItem(user) { serverId, userId ->
          navController.navigate(MeteroidScreen.Home.Purchase.build(serverId, userId)) {
            launchSingleTop = true
            restoreState = false
            popUpTo(findStartDestination(navController.graph).id) {
              saveState = false
            }
          }
          viewModel.selectUser(serverId, userId)
        }
      }
    }

    for (character in 'A'..'Z') {
      val group = users.filter { it.name.startsWith(character, ignoreCase = true) }
      if (group.isNotEmpty()) {
        stickyHeader(character.toString()) {
          ListItem(headlineContent = {
            Text(
              "$character",
              style = MaterialTheme.typography.labelMedium,
              modifier = Modifier.padding(start = 8.dp, end = 8.dp, top = 16.dp, bottom = 4.dp)
            )
          })
        }

        items(
          group,
          key = { "user-${it.serverId}-${it.userId}" },
        ) { user ->
          UserListItem(user) { serverId, userId ->
            navController.navigate(MeteroidScreen.Home.Purchase.build(serverId, userId)) {
              launchSingleTop = true
              restoreState = false
              popUpTo(findStartDestination(navController.graph).id) {
                saveState = false
              }
            }
            viewModel.selectUser(serverId, userId)
          }
        }
      }
    }
  }
}
