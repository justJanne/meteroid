/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import de.chaosdorf.meteroid.ui.MeteroidScreen
import de.chaosdorf.meteroid.ui.navigation.NavigationViewModel
import de.chaosdorf.meteroid.util.findStartDestination
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@Composable
fun MeteroidBottomBar(navController: NavController, viewModel: NavigationViewModel) {
  val backStackEntry by navController.currentBackStackEntryAsState()
  val activeRoute = MeteroidScreen.byRoute(backStackEntry?.destination?.route)

  val account by viewModel.account.collectAsState()
  val server = account?.server
  val user = account?.user
  val historyDisabled by viewModel.historyDisabled.collectAsState()
  val wrappedEnabled = Clock.System.now()
    .toLocalDateTime(TimeZone.currentSystemDefault())
    .month.let { it == java.time.Month.DECEMBER }

  AnimatedVisibility(
    activeRoute is MeteroidScreen.Home && server != null && user != null,
    enter = slideInVertically(initialOffsetY = { it }),
    exit = slideOutVertically(targetOffsetY = { it })
  ) {
    NavigationBar(
      contentColor = MaterialTheme.colorScheme.primary
    ) {
      MeteroidBottomBarItem(
        MeteroidScreen.Home.Purchase,
        activeRoute == MeteroidScreen.Home.Purchase,
      ) {
        if (server != null && user != null) {
          navController.navigate(MeteroidScreen.Home.Purchase.build(server, user)) {
            launchSingleTop = true
            restoreState = false
            popUpTo(findStartDestination(navController.graph).id) {
              saveState = false
            }
          }
        }
      }

      MeteroidBottomBarItem(
        MeteroidScreen.Home.Deposit,
        activeRoute == MeteroidScreen.Home.Deposit,
      ) {
        if (server != null && user != null) {
          navController.navigate(MeteroidScreen.Home.Deposit.build(server, user)) {
            launchSingleTop = true
            restoreState = false
            popUpTo(findStartDestination(navController.graph).id) {
              saveState = false
            }
          }
        }
      }

      if (!historyDisabled) {
        MeteroidBottomBarItem(
          MeteroidScreen.Home.History,
          activeRoute == MeteroidScreen.Home.History,
        ) {
          if (server != null && user != null) {
            navController.navigate(MeteroidScreen.Home.History.build(server, user)) {
              launchSingleTop = true
              restoreState = false
              popUpTo(findStartDestination(navController.graph).id) {
                saveState = false
              }
            }
          }
        }

        if (wrappedEnabled) {
          MeteroidBottomBarItem(
            MeteroidScreen.Home.Wrapped,
            activeRoute == MeteroidScreen.Home.Wrapped,
          ) {
            if (server != null && user != null) {
              navController.navigate(MeteroidScreen.Home.Wrapped.build(server, user)) {
                launchSingleTop = true
                restoreState = false
                popUpTo(findStartDestination(navController.graph).id) {
                  saveState = false
                }
              }
            }
          }
        }
      }
    }
  }
}
