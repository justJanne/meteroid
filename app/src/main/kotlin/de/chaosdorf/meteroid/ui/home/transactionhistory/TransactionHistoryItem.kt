/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home.transactionhistory

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AttachMoney
import androidx.compose.material.icons.filled.QuestionMark
import androidx.compose.material3.Icon
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import de.chaosdorf.meteroid.model.Drink
import de.chaosdorf.meteroid.model.Transaction
import de.chaosdorf.meteroid.theme.secondaryGradient
import de.chaosdorf.meteroid.ui.home.PriceBadge
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toLocalDateTime
import java.math.BigDecimal
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@Composable
fun TransactionHistoryItem(
  transaction: Transaction,
  drink: Drink?,
  modifier: Modifier = Modifier
) {
  val timestamp = transaction.timestamp.toLocalDateTime(TimeZone.currentSystemDefault())
  val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)

  ListItem(
    headlineContent = {
      val label = when {
        drink != null -> drink.name
        transaction.difference > BigDecimal.ZERO -> "Deposit"
        else -> "Unknown"
      }
      Text(label)
    },
    supportingContent = {
      Text(formatter.format(timestamp.toJavaLocalDateTime()))
    },
    leadingContent = {
      Box(
        modifier = Modifier
          .size(48.dp)
          .clip(CircleShape)
          .aspectRatio(1.0f)
          .background(MaterialTheme.colorScheme.secondaryGradient.verticalGradient())
      ) {
        if (drink != null) {
          val thumbPainter = rememberAsyncImagePainter(
            drink.logoUrl
          )
          val originalPainter = rememberAsyncImagePainter(
            drink.originalLogoUrl,
            error = thumbPainter
          )

          Image(
            painter = originalPainter,
            contentDescription = null,
            contentScale = ContentScale.Fit,
            modifier = Modifier
              .align(Alignment.Center)
              .fillMaxSize()
          )
        } else if (transaction.difference > BigDecimal.ZERO) {
          Icon(
            Icons.Default.AttachMoney,
            contentDescription = null,
            modifier = Modifier.align(Alignment.Center)
          )
        } else {
          Icon(
            Icons.Default.QuestionMark,
            contentDescription = null,
            modifier = Modifier.align(Alignment.Center)
          )
        }
      }
    },
    trailingContent = {
      PriceBadge(
        transaction.difference,
        modifier = Modifier.padding(horizontal = 8.dp)
      )
    },
    modifier = modifier,
  )
}
