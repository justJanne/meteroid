/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.servers

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import kotlinx.coroutines.launch
import okhttp3.HttpUrl.Companion.toHttpUrl

@Composable
fun AddServerScreen(
  navController: NavController,
  viewModel: AddServerViewModel,
  contentPadding: PaddingValues = PaddingValues(),
) {
  val scope = rememberCoroutineScope()
  val url by viewModel.url.collectAsState()
  val server by viewModel.server.collectAsState()

  Column(
    Modifier
      .padding(contentPadding)
      .padding(16.dp, 8.dp)
  ) {
    TextField(
      label = { Text("Server URL") },
      value = url,
      onValueChange = { viewModel.url.value = it },
      modifier = Modifier.fillMaxWidth()
    )

    server?.let { server ->
      Card(
        modifier = Modifier.padding(vertical = 8.dp)
      ) {
        Row(modifier = Modifier.padding(16.dp, 8.dp)) {
          AsyncImage(
            server.logoUrl,
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier.size(48.dp)
          )
          Spacer(Modifier.width(16.dp))
          Column(modifier = Modifier.align(Alignment.CenterVertically)) {
            Text(
              server.name!!,
              fontWeight = FontWeight.SemiBold,
              style = MaterialTheme.typography.bodyMedium
            )
            Text(
              server.url.toHttpUrl().host,
              color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.67f),
              fontWeight = FontWeight.Medium,
              style = MaterialTheme.typography.bodyMedium
            )
          }

          Spacer(
            Modifier
              .width(16.dp)
              .weight(1.0f)
          )

          IconButton(onClick = {
            scope.launch {
              viewModel.addServer()
              navController.navigateUp()
            }
          }) {
            Icon(Icons.Default.Add, contentDescription = "Add Server")
          }
        }
      }
    }
  }
}
