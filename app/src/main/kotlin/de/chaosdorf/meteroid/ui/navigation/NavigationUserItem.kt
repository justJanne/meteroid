/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.navigation

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.StarOutline
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ListItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.model.User
import de.chaosdorf.meteroid.ui.UserAvatar
import de.chaosdorf.meteroid.ui.home.PriceBadge

@Composable
fun NavigationUserItem(
  expanded: Boolean,
  user: User,
  pinned: Boolean,
  onTogglePin: (ServerId, UserId) -> Unit,
  onExpand: () -> Unit,
  onClick: () -> Unit,
) {
  ListItem(
    headlineContent = { Text(user.name, maxLines = 1, overflow = TextOverflow.Ellipsis) },
    supportingContent = { if (user.email != null) Text(user.email!!, maxLines = 1, overflow = TextOverflow.Ellipsis) },
    leadingContent = { UserAvatar(user.gravatarUrl) },
    trailingContent = {
      Row(verticalAlignment = Alignment.CenterVertically) {
        AnimatedVisibility(!expanded, enter = fadeIn(), exit = fadeOut()) {
          IconButton(onClick = { onTogglePin(user.serverId, user.userId) }) {
            Icon(
              if (pinned) Icons.Default.Star else Icons.Default.StarOutline,
              contentDescription = null
            )
          }
        }
        PriceBadge(user.balance)
      }
    },
    modifier = Modifier.requiredHeight(64.dp)
      .clickable(onClick = if (expanded) onClick else onExpand)
  )
}
