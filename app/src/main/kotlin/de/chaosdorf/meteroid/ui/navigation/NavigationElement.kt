/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.navigation

import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.model.Server
import de.chaosdorf.meteroid.model.User
import de.chaosdorf.meteroid.ui.MeteroidScreen

sealed class NavigationElement(val key: String) {
  abstract fun isCurrent(route: MeteroidScreen?, serverId: ServerId?, userId: UserId?): Boolean

  data class ServerElement(
    val server: Server,
  ) : NavigationElement("navigation-${server.serverId}") {

    override fun isCurrent(route: MeteroidScreen?, serverId: ServerId?, userId: UserId?): Boolean {
      return route == MeteroidScreen.UserList && serverId == server.serverId
    }
  }

  data class UserElement(
    val user: User,
    val pinned: Boolean,
  ) : NavigationElement("navigation-${user.serverId}-${user.userId}-${if (pinned) "pinned" else "current"}") {
    override fun isCurrent(route: MeteroidScreen?, serverId: ServerId?, userId: UserId?): Boolean {
      return route is MeteroidScreen.Home && serverId == user.serverId && userId == user.userId
    }
  }

  data class UserListElement(
    val server: Server,
  ) : NavigationElement("navigation-${server.serverId}-userList") {
    override fun isCurrent(route: MeteroidScreen?, serverId: ServerId?, userId: UserId?): Boolean = false
  }

  data object AddServerElement : NavigationElement("navigation-addServer") {

    override fun isCurrent(route: MeteroidScreen?, serverId: ServerId?, userId: UserId?): Boolean {
      return route == MeteroidScreen.AddServer
    }
  }

  data object SettingsElement : NavigationElement("navigation-settings") {

    override fun isCurrent(route: MeteroidScreen?, serverId: ServerId?, userId: UserId?): Boolean {
      return route == MeteroidScreen.Settings
    }
  }
}
