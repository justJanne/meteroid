/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home.purchase

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun PurchaseScreen(
  navController: NavController,
  viewModel: PurchaseViewModel,
  contentPadding: PaddingValues = PaddingValues(),
) {
  val drinks by viewModel.drinks.collectAsState()
  val filters by viewModel.filters.collectAsState()

  LazyVerticalGrid(
    GridCells.Adaptive(104.dp),
    contentPadding = contentPadding,
    modifier = Modifier.padding(horizontal = 8.dp),
  ) {
    item("filter", span = { GridItemSpan(maxLineSpan) }) {
      FlowRow(
        modifier = Modifier.padding(horizontal = 16.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp)
      ) {
        PurchaseFilterChip(
          label = "Active",
          selected = filters.contains(PurchaseViewModel.Filter.Active),
          onClick = { viewModel.toggleFilter(PurchaseViewModel.Filter.Active) }
        )
        PurchaseFilterChip(
          label = "Coffeine Free",
          selected = filters.contains(PurchaseViewModel.Filter.CaffeineFree),
          onClick = { viewModel.toggleFilter(PurchaseViewModel.Filter.CaffeineFree) }
        )
      }
    }

    items(
      drinks,
      key = { "drink-${it.serverId}-${it.drinkId}" },
    ) { drink ->
      PurchaseDrinkTile(drink) { item, count ->
        viewModel.purchase(item, count, navController::navigateUp)
      }
    }
  }
}
