/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import de.chaosdorf.meteroid.theme.icons.MeteroidIcons
import de.chaosdorf.meteroid.theme.icons.filled.WaterFull

@Preview
@Composable
fun UserAvatar(
  source: String? = null,
) {
  var success by remember { mutableStateOf(false) }

  AvatarLayout(
    Modifier.clip(CircleShape)
      .background(MaterialTheme.colorScheme.primaryContainer)
  ) {
    if (!success) {
      Icon(
        Icons.Filled.Person,
        contentDescription = null,
        tint = MaterialTheme.colorScheme.primary
      )
    }
    AsyncImage(
      source,
      contentDescription = null,
      contentScale = ContentScale.Crop,
      modifier = Modifier.fillMaxSize(),
      onSuccess = { success = true },
      onError = { success = false },
      onLoading = { success = false },
    )
  }
}

@Preview
@Composable
fun ServerAvatar(
  source: String? = null,
) {
  var success by remember { mutableStateOf(false) }

  AvatarLayout {
    if (!success) {
      Icon(
        MeteroidIcons.Filled.WaterFull,
        contentDescription = null
      )
    }
    AsyncImage(
      source,
      contentDescription = null,
      contentScale = ContentScale.Crop,
      modifier = Modifier.fillMaxSize(),
      onSuccess = { success = true },
      onError = { success = false },
      onLoading = { success = false },
    )
  }
}

@Composable
fun AvatarLayout(
  modifier: Modifier = Modifier,
  content: @Composable () -> Unit
) {
  Box(
    modifier.size(36.dp),
    contentAlignment = Alignment.Center
  ) {
    content()
  }
}
