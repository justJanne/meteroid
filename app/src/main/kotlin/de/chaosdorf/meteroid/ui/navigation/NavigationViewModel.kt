/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.navigation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.model.*
import de.chaosdorf.meteroid.storage.AccountPreferences
import de.chaosdorf.meteroid.sync.AccountProvider
import de.chaosdorf.meteroid.sync.SyncManager
import de.chaosdorf.meteroid.sync.base.SyncHandler
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NavigationViewModel @Inject constructor(
  serverRepository: ServerRepository,
  userRepository: UserRepository,
  pinnedUserRepository: PinnedUserRepository,
  syncManager: SyncManager,
  private val accountProvider: AccountProvider,
  private val preferences: AccountPreferences
) : ViewModel() {
  val expanded = MutableStateFlow(false)
  val account = MutableStateFlow<AccountPreferences.State?>(null)

  val syncState = syncManager.syncState
    .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), SyncHandler.State.Idle)

  private val servers: StateFlow<List<Server>> = serverRepository.getAllFlow()
    .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())

  private val pinnedUsers: StateFlow<List<User>> = pinnedUserRepository.getPinnedUsersFlow()
    .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())

  private val currentUser: StateFlow<User?> = account.flatMapLatest { account ->
    if (account?.server == null || account.user == null) flowOf(null)
    else userRepository.getFlow(account.server, account.user)
  }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), null)

  val historyDisabled = currentUser.map {
    it?.audit == false
  }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), false)

  val entries: StateFlow<List<NavigationElement>> = combine(
    servers,
    pinnedUsers,
    currentUser,
  ) { servers, pinnedUsers, currentUser ->
    val entries = mutableListOf<NavigationElement>()
    val userInList = currentUser != null && pinnedUsers.any {
      it.serverId == currentUser.serverId && it.userId == currentUser.userId
    }

    for (server in servers) {
      entries.add(NavigationElement.ServerElement(server))
      if (currentUser != null && currentUser.serverId == server.serverId && !userInList) {
        entries.add(NavigationElement.UserElement(currentUser, pinned = false))
      }
      for (user in pinnedUsers) {
        if (user.serverId == server.serverId) {
          entries.add(NavigationElement.UserElement(user, pinned = true))
        }
      }
      entries.add(NavigationElement.UserListElement(server))
    }

    entries.add(NavigationElement.AddServerElement)
    entries.add(NavigationElement.SettingsElement)

    entries
  }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())

  init {
    viewModelScope.launch {
      account.collectLatest { account ->
        val serverId = account?.server
        val userId = account?.user
        if (serverId != null && userId != null) {
          val server = serverRepository.get(serverId)
          val user = userRepository.get(serverId, userId)
          if (server != null && user != null) {
            syncManager.sync(server, user, incremental = true)
          }
        }
      }
    }
  }

  fun togglePin(serverId: ServerId, userId: UserId) {
    viewModelScope.launch {
      accountProvider.togglePin(serverId, userId)
    }
  }

  fun selectUser(serverId: ServerId, userId: UserId) {
    Log.i("UserListViewModel", "Updating AccountPreferences: $serverId $userId")
    viewModelScope.launch {
      preferences.setUser(serverId, userId)
    }
  }
}
