/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home.deposit

import androidx.annotation.DrawableRes
import de.chaosdorf.meteroid.R
import java.math.BigDecimal

enum class MonetaryAmount(val amount: BigDecimal, @DrawableRes val image: Int) {
  MONEY_50(0.50.toBigDecimal(), R.drawable.euro_50),
  MONEY_100(1.00.toBigDecimal(), R.drawable.euro_100),
  MONEY_200(2.00.toBigDecimal(), R.drawable.euro_200),
  MONEY_500(5.00.toBigDecimal(), R.drawable.euro_500),
  MONEY_1000(10.00.toBigDecimal(), R.drawable.euro_1000),
  MONEY_2000(20.00.toBigDecimal(), R.drawable.euro_2000),
  MONEY_5000(50.00.toBigDecimal(), R.drawable.euro_5000),
}
