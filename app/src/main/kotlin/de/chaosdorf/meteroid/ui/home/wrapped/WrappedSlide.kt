/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home.wrapped

import de.chaosdorf.mete.model.DrinkId
import de.chaosdorf.meteroid.model.Drink
import de.chaosdorf.meteroid.model.Transaction
import kotlinx.datetime.DayOfWeek
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

sealed class WrappedSlide {
  interface Factory {
    fun create(
      transactions: List<Transaction>,
      drinks: Map<DrinkId, Drink>
    ): WrappedSlide?
  }

  data class MostBoughtDrink(
    val drink: Drink,
    val count: Int,
  ) : WrappedSlide() {
    companion object : Factory {
      override fun create(
        transactions: List<Transaction>,
        drinks: Map<DrinkId, Drink>
      ): WrappedSlide? = transactions
        .mapNotNull { drinks[it.drinkId] }
        .groupingBy { it }
        .eachCount()
        .maxByOrNull { it.value }
        ?.let { (mostBoughtDrink, count) ->
          MostBoughtDrink(mostBoughtDrink, count)
        }
    }
  }

  data class Caffeine(
    val total: Double,
    val wouldKill: Animal?,
  ) : WrappedSlide() {
    enum class Animal(val lethalDosage: Double) {
      Hamster(0.25),
      Squirrel(0.3),
      Rat(0.4),
      GuineaPig(0.9),
      Lemur(2.5),
      Cat(5.0),
      Koala(9.0),
      Coyote(13.0),
      Lynx(23.0),
      Capybara(55.0),
      Jaguar(81.0),
      Reindeer(101.0),
      Gorilla(140.0),
      Lion(175.0),
      Bear(278.0),
      Moose(368.0),
      Bison(540.0)
    }

    companion object : Factory {
      override fun create(
        transactions: List<Transaction>,
        drinks: Map<DrinkId, Drink>
      ): WrappedSlide = transactions
        .mapNotNull { drinks[it.drinkId] }
        .mapNotNull { drink -> drink.caffeine?.let { it * drink.volume.toDouble() * 10 } }
        .sum()
        .let { dosage ->
          Caffeine(
            dosage,
            Animal.entries
              .sortedBy(Animal::lethalDosage)
              .lastOrNull { it.lethalDosage < dosage }
          )
        }
    }
  }

  data class MostActive(
    val weekday: DayOfWeek,
    val hour: Int,
  ) : WrappedSlide() {
    companion object : Factory {
      override fun create(
        transactions: List<Transaction>,
        drinks: Map<DrinkId, Drink>
      ): WrappedSlide? = transactions
        .map { it.timestamp.toLocalDateTime(TimeZone.currentSystemDefault()) }
        .groupingBy { Pair(it.dayOfWeek, it.hour) }
        .eachCount()
        .maxByOrNull { it.value }
        ?.key
        ?.let { (dayOfWeek, hour) ->
          MostActive(dayOfWeek, hour)
        }
    }
  }
}
