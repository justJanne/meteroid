/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home.purchase

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import de.chaosdorf.meteroid.model.Drink
import de.chaosdorf.meteroid.theme.secondaryGradient
import de.chaosdorf.meteroid.ui.home.PriceBadge
import kotlinx.coroutines.delay
import java.math.BigDecimal

@Composable
fun PurchaseDrinkTile(
  item: Drink,
  modifier: Modifier = Modifier,
  onPurchase: (Drink, Int) -> Unit = { _, _ -> }
) {
  var purchaseCount by remember { mutableStateOf(0) }
  val pendingPurchases = purchaseCount != 0

  LaunchedEffect(purchaseCount) {
    delay(2000L)
    onPurchase(item, purchaseCount)
    purchaseCount = 0
  }

  val thumbPainter = rememberAsyncImagePainter(
    item.logoUrl
  )
  val drinkPainter = rememberAsyncImagePainter(
    item.originalLogoUrl,
    error = thumbPainter
  )

  Column(
    modifier = modifier
      .height(IntrinsicSize.Max)
      .alpha(if (item.active) 1.0f else 0.67f)
      .clip(RoundedCornerShape(8.dp))
      .clickable { purchaseCount += 1 }
      .padding(8.dp)
  ) {
    Box(
      Modifier.aspectRatio(1.0f)
        .background(MaterialTheme.colorScheme.secondaryGradient.verticalGradient(), CircleShape),
      contentAlignment = Alignment.Center
    ) {
      Image(
        drinkPainter,
        contentDescription = null,
        contentScale = ContentScale.Fit,
        modifier = Modifier.alpha(if (pendingPurchases) 0.0f else 1.0f)
          .clip(CircleShape)
      )
      PriceBadge(
        item.price,
        modifier = Modifier
          .alpha(if (pendingPurchases) 0.0f else 1.0f)
          .align(Alignment.BottomEnd)
          .paddingFromBaseline(bottom = 12.dp)
      )
      Text(
        "×$purchaseCount",
        fontSize = 36.sp,
        fontWeight = FontWeight.Light,
        color = MaterialTheme.colorScheme.onSecondaryContainer.copy(alpha = 0.67f),
        textAlign = TextAlign.Center,
        modifier = Modifier.alpha(if (pendingPurchases) 1.0f else 0.0f)
      )
    }
    Spacer(Modifier.height(4.dp))
    Text(
      item.name,
      modifier = Modifier
        .fillMaxWidth()
        .padding(horizontal = 8.dp),
      textAlign = TextAlign.Center,
      fontWeight = FontWeight.SemiBold,
      style = MaterialTheme.typography.labelLarge,
    )
    Spacer(Modifier.height(4.dp))
    Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
      val unitPrice =
        if (item.volume <= BigDecimal.ZERO) null
        else item.price / item.volume

      Text(
        if (unitPrice == null) String.format("%.02fl", item.volume)
        else String.format("%.02fl · %.02f€/l", item.volume, item.price / item.volume),
        modifier = Modifier
          .fillMaxWidth()
          .padding(horizontal = 8.dp),
        textAlign = TextAlign.Center,
        fontWeight = FontWeight.SemiBold,
        style = MaterialTheme.typography.labelMedium,
        color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.38f)
      )
    }
  }
}
