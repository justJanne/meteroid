/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui

import android.util.Log
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.storage.AccountPreferences
import de.chaosdorf.meteroid.ui.home.deposit.DepositScreen
import de.chaosdorf.meteroid.ui.home.deposit.DepositViewModel
import de.chaosdorf.meteroid.ui.home.purchase.PurchaseScreen
import de.chaosdorf.meteroid.ui.home.purchase.PurchaseViewModel
import de.chaosdorf.meteroid.ui.home.transactionhistory.TransactionHistoryScreen
import de.chaosdorf.meteroid.ui.home.transactionhistory.TransactionHistoryViewModel
import de.chaosdorf.meteroid.ui.home.wrapped.WrappedScreen
import de.chaosdorf.meteroid.ui.home.wrapped.WrappedViewModel
import de.chaosdorf.meteroid.ui.navigation.NavigationViewModel
import de.chaosdorf.meteroid.ui.servers.AddServerScreen
import de.chaosdorf.meteroid.ui.servers.AddServerViewModel
import de.chaosdorf.meteroid.ui.settings.SettingsScreen
import de.chaosdorf.meteroid.ui.settings.SettingsViewModel
import de.chaosdorf.meteroid.ui.userlist.UserListScreen
import de.chaosdorf.meteroid.ui.userlist.UserListViewModel
import de.chaosdorf.meteroid.util.toFancyString
import kotlinx.coroutines.flow.collectLatest

@Composable
fun MeteroidRouter(
  initialAccount: AccountPreferences.State,
) {
  val navController = rememberNavController()
  val navigationViewModel = hiltViewModel<NavigationViewModel>()

  LaunchedEffect(navController) {
    navController.currentBackStack.collectLatest {
      Log.i("MeteroidRouter", "Navigation: ${it.toFancyString()}")
    }
  }

  MeteroidScaffold(navController, navigationViewModel) { paddingValues ->
    NavHost(
      navController = navController,
      startDestination = MeteroidScreen.Home.Purchase.route,
      enterTransition = { fadeIn() },
      exitTransition = { fadeOut() },
      popEnterTransition = { fadeIn() },
      popExitTransition = { fadeOut() },
      modifier = Modifier.padding(paddingValues)
    ) {
      composable(
        MeteroidScreen.Home.Purchase.route, arguments = listOf(
          navArgument("server") {
            type = NavType.LongType
            defaultValue = initialAccount.server?.value ?: -1L
          },
          navArgument("user") {
            type = NavType.LongType
            defaultValue = initialAccount.user?.value ?: -1L
          },
        )
      ) { entry ->
        val serverId = entry.arguments?.getLong("server")?.let(::ServerId)
          ?: ServerId(-1L)
        val userId = entry.arguments?.getLong("user")?.let(::UserId)
          ?: UserId(-1L)

        LaunchedEffect(serverId, userId) {
          if (!serverId.isValid() || !userId.isValid()) {
            navigationViewModel.expanded.value = true
          }
        }

        if (serverId.isValid() && userId.isValid()) {
          val viewModel: PurchaseViewModel = hiltViewModel(
            key = MeteroidScreen.Home.Purchase.build(serverId, userId)
          )
          PurchaseScreen(navController, viewModel, PaddingValues(top = 96.dp))
        }
      }
      composable(
        MeteroidScreen.Home.Deposit.route, arguments = listOf(
          navArgument("server") {
            type = NavType.LongType
          },
          navArgument("user") {
            type = NavType.LongType
          },
        )
      ) {
        val viewModel: DepositViewModel = hiltViewModel()
        DepositScreen(navController, viewModel, PaddingValues(top = 96.dp))
      }
      composable(
        MeteroidScreen.Home.History.route, arguments = listOf(
          navArgument("server") {
            type = NavType.LongType
          },
          navArgument("user") {
            type = NavType.LongType
          },
        )
      ) {
        val viewModel: TransactionHistoryViewModel = hiltViewModel()
        TransactionHistoryScreen(viewModel, PaddingValues(top = 96.dp))
      }
      composable(
        MeteroidScreen.Home.Wrapped.route, arguments = listOf(
          navArgument("server") {
            type = NavType.LongType
          },
          navArgument("user") {
            type = NavType.LongType
          },
        )
      ) {
        val viewModel: WrappedViewModel = hiltViewModel()
        WrappedScreen(viewModel, PaddingValues(top = 96.dp))
      }
      composable(
        MeteroidScreen.UserList.route, arguments = listOf(
          navArgument("server") {
            type = NavType.LongType
          },
        )
      ) {
        val viewModel: UserListViewModel = hiltViewModel()
        UserListScreen(navController, viewModel, PaddingValues(top = 96.dp))
      }
      composable(MeteroidScreen.AddServer.route) {
        val viewModel: AddServerViewModel = hiltViewModel()
        AddServerScreen(navController, viewModel, PaddingValues(top = 96.dp))
      }
      composable(MeteroidScreen.Settings.route) {
        val viewModel: SettingsViewModel = hiltViewModel()
        SettingsScreen(navController, viewModel, PaddingValues(top = 96.dp))
      }
    }
  }
}
