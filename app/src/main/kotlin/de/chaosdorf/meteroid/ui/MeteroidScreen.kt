/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Celebration
import androidx.compose.material.icons.outlined.History
import androidx.compose.material.icons.outlined.LocalAtm
import androidx.compose.material.icons.twotone.Celebration
import androidx.compose.material.icons.twotone.History
import androidx.compose.material.icons.twotone.LocalAtm
import androidx.compose.ui.graphics.vector.ImageVector
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.theme.icons.MeteroidIcons
import de.chaosdorf.meteroid.theme.icons.outlined.WaterFull
import de.chaosdorf.meteroid.theme.icons.twotone.WaterFull

sealed class MeteroidScreen(val route: String) {
  sealed class Home(
    val label: String,
    val activeIcon: ImageVector,
    val inactiveIcon: ImageVector,
    route: String
  ) : MeteroidScreen(route) {
    data object Purchase : Home(
      "Purchase",
      MeteroidIcons.TwoTone.WaterFull,
      MeteroidIcons.Outlined.WaterFull,
      "server/{server}/user/{user}/purchase"
    ) {
      fun build(server: ServerId, user: UserId) = route
        .replace("{server}", server.value.toString())
        .replace("{user}", user.value.toString())
    }

    data object Deposit : Home(
      "Deposit",
      Icons.TwoTone.LocalAtm,
      Icons.Outlined.LocalAtm,
      "server/{server}/user/{user}/deposit"
    ) {
      fun build(server: ServerId, user: UserId) = route
        .replace("{server}", server.value.toString())
        .replace("{user}", user.value.toString())
    }

    data object History : Home(
      "History",
      Icons.TwoTone.History,
      Icons.Outlined.History,
      "server/{server}/user/{user}/history"
    ) {
      fun build(server: ServerId, user: UserId) = route
        .replace("{server}", server.value.toString())
        .replace("{user}", user.value.toString())
    }

    data object Wrapped : Home(
      "Wrapped",
      Icons.TwoTone.Celebration,
      Icons.Outlined.Celebration,
      "server/{server}/user/{user}/wrapped"
    ) {
      fun build(server: ServerId, user: UserId) = route
        .replace("{server}", server.value.toString())
        .replace("{user}", user.value.toString())
    }
  }

  data object UserList : MeteroidScreen("server/{server}/userList") {
    fun build(server: ServerId) = route
      .replace("{server}", server.value.toString())
  }

  data object AddServer : MeteroidScreen("addServer") {
    fun build() = route
  }

  data object Settings : MeteroidScreen("settings") {
    fun build() = route
  }

  companion object {
    fun byRoute(route: String?) = when (route) {
      Home.Purchase.route -> Home.Purchase
      Home.Deposit.route -> Home.Deposit
      Home.History.route -> Home.History
      Home.Wrapped.route -> Home.Wrapped
      UserList.route -> UserList
      AddServer.route -> AddServer
      Settings.route -> Settings
      else -> null
    }
  }
}
