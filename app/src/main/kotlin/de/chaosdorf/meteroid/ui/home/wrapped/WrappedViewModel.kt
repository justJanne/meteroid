/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home.wrapped

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.chaosdorf.mete.model.DrinkId
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.meteroid.model.Drink
import de.chaosdorf.meteroid.model.DrinkRepository
import de.chaosdorf.meteroid.model.Transaction
import de.chaosdorf.meteroid.model.TransactionRepository
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.datetime.*
import javax.inject.Inject

@HiltViewModel
class WrappedViewModel @Inject constructor(
  savedStateHandle: SavedStateHandle,
  repository: TransactionRepository,
  drinkRepository: DrinkRepository,
) : ViewModel() {
  private val serverId = ServerId(checkNotNull(savedStateHandle["server"]))
  private val userId = UserId(checkNotNull(savedStateHandle["user"]))

  private fun List<Transaction>.filterAudits(year: Int): List<Transaction> {
    val yearBegin = LocalDateTime(year, Month.JANUARY, 1, 0, 0, 0)
      .toInstant(TimeZone.UTC)
    val yearEnd = LocalDateTime(year, Month.DECEMBER, 31, 23, 59, 59)
      .toInstant(TimeZone.UTC)
    return this.filter {
      it.timestamp in yearBegin..yearEnd
    }
  }

  val slides: StateFlow<List<WrappedSlide>> = combine(
    repository.getAllFlow(serverId, userId),
    drinkRepository.getAllFlow(serverId)
  ) { transactions, drinks ->
    val drinkMap: Map<DrinkId, Drink> = drinks.associateBy(Drink::drinkId)
    val factories = listOf(
      WrappedSlide.MostBoughtDrink,
      WrappedSlide.Caffeine,
      WrappedSlide.MostActive
    )
    val now = Clock.System.now().toLocalDateTime(TimeZone.UTC)
    val content = transactions.filterAudits(now.year)
    factories.mapNotNull { it.create(content, drinkMap) }
  }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())
}
