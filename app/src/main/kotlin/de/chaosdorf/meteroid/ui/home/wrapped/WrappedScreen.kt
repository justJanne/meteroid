/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.ui.home.wrapped

import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ListItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import de.chaosdorf.meteroid.R
import java.time.format.TextStyle

@Composable
fun WrappedScreen(
  viewModel: WrappedViewModel,
  contentPadding: PaddingValues = PaddingValues(),
) {
  val slides by viewModel.slides.collectAsState()
  @Suppress("DEPRECATION")
  val locale = LocalConfiguration.current.let {
    if (VERSION.SDK_INT >= VERSION_CODES.N) it.locales.get(0)
    else it.locale
  }

  LazyColumn(contentPadding = contentPadding) {
    items(
      slides,
      key = { "wrapped-${it::class.simpleName}" },
    ) { slide ->
      when (slide) {
        is WrappedSlide.MostBoughtDrink ->
          ListItem(
            headlineContent = {
              Text("Your favorite drink is ${slide.drink.name}")
            },
            supportingContent = {
              Text("At least you enjoyed it ${slide.count} times this year.")
            },
            leadingContent = {
              val thumbPainter = rememberAsyncImagePainter(
                slide.drink.logoUrl
              )
              val drinkPainter = rememberAsyncImagePainter(
                slide.drink.originalLogoUrl,
                error = thumbPainter
              )

              Image(
                drinkPainter,
                contentDescription = null,
                contentScale = ContentScale.Fit,
                modifier = Modifier.size(72.dp)
              )
            }
          )

        is WrappedSlide.Caffeine ->
          ListItem(
            headlineContent = {
              Text("You consumed ${slide.total} mg of caffeine this year.")
            },
            supportingContent = {
              slide.wouldKill?.let { animal ->
                Text("This could kill a medium-weight ${animal.name}. Glad you're still here.")
              }
            },
            leadingContent = {
              val painter = painterResource(
                when (slide.wouldKill) {
                  WrappedSlide.Caffeine.Animal.Squirrel -> R.drawable.wrapped_squirrel
                  WrappedSlide.Caffeine.Animal.Rat -> R.drawable.wrapped_rat
                  WrappedSlide.Caffeine.Animal.Cat -> R.drawable.wrapped_cat
                  WrappedSlide.Caffeine.Animal.Koala -> R.drawable.wrapped_koala
                  WrappedSlide.Caffeine.Animal.Lynx -> R.drawable.wrapped_lynx
                  WrappedSlide.Caffeine.Animal.Jaguar -> R.drawable.wrapped_jaguar
                  WrappedSlide.Caffeine.Animal.Reindeer -> R.drawable.wrapped_reindeer
                  WrappedSlide.Caffeine.Animal.Gorilla -> R.drawable.wrapped_gorilla
                  WrappedSlide.Caffeine.Animal.Lion -> R.drawable.wrapped_lion
                  WrappedSlide.Caffeine.Animal.Bear -> R.drawable.wrapped_bear
                  WrappedSlide.Caffeine.Animal.Moose -> R.drawable.wrapped_moose
                  else -> R.drawable.wrapped_coffee_beans
                }
              )

              Image(
                painter,
                contentDescription = null,
                contentScale = ContentScale.Fit,
                modifier = Modifier.size(72.dp)
              )
            }
          )

        is WrappedSlide.MostActive ->
          ListItem(
            headlineContent = {
              Text(
                "You were most active on ${
                  slide.weekday.getDisplayName(TextStyle.FULL, locale)
                }s at ${slide.hour} o'clock."
              )
            },
            leadingContent = {
              val painter = painterResource(R.drawable.wrapped_clock)

              Image(
                painter,
                contentDescription = null,
                contentScale = ContentScale.Fit,
                modifier = Modifier.size(72.dp)
              )
            }
          )
      }
    }
  }
}
