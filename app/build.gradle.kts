@file:Suppress("UnstableApiUsage")

/*
 * Quasseldroid - Quassel client for Android
 *
 * Copyright (c) 2019 Janne Mareike Koschinski
 * Copyright (c) 2019 The Quassel Project
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

plugins {
  id("justjanne.android.app")
  alias(libs.plugins.kotlin.serialization)
  alias(libs.plugins.kotlin.ksp)
  alias(libs.plugins.dagger.hilt)
}

android {
  namespace = "de.chaosdorf.meteroid"

  buildTypes {
    getByName("release") {

      proguardFiles(
        getDefaultProguardFile("proguard-android.txt"),
        "proguard-rules.pro"
      )
      signingConfig = signingConfigs.getByName("debug")
    }

    getByName("debug") {
      applicationIdSuffix = ".debug"
    }
  }

  buildFeatures {
    compose = true
  }

  composeOptions {
    kotlinCompilerExtensionVersion = libs.versions.androidx.compose.compiler.get()
  }
}

dependencies {
  implementation(libs.kotlin.stdlib)
  implementation(libs.kotlinx.datetime)
  implementation(libs.kotlinx.serialization.json)
  coreLibraryDesugaring(libs.desugar.jdk)

  implementation(libs.kotlinx.coroutines.android)
  testImplementation(libs.kotlinx.coroutines.test)

  testImplementation(libs.kotlin.test)
  testImplementation(libs.junit.api)
  testImplementation(libs.junit.params)
  testRuntimeOnly(libs.junit.engine)

  implementation(libs.androidx.appcompat)
  implementation(libs.androidx.appcompat.resources)

  implementation(libs.androidx.activity)
  implementation(libs.androidx.activity.compose)

  implementation(libs.androidx.splashscreen)

  implementation(libs.androidx.compose.animation)
  implementation(libs.androidx.compose.compiler)
  implementation(libs.androidx.compose.foundation)
  implementation(libs.androidx.compose.material)
  implementation(libs.androidx.compose.material.icons)
  implementation(libs.androidx.compose.runtime)
  implementation(libs.androidx.compose.ui.tooling)

  implementation(libs.androidx.room.runtime)
  implementation(libs.androidx.room.ktx)
  implementation(libs.androidx.room.paging)

  implementation(libs.androidx.navigation.compose)

  implementation(libs.okhttp)
  implementation(libs.coil.compose)

  implementation(libs.hilt.navigation)
  implementation(libs.hilt.android)
  ksp(libs.hilt.compiler)

  implementation(libs.androidx.datastore.preferences)

  implementation(project(":api"))
  implementation(project(":persistence"))

  debugImplementation(libs.androidx.compose.ui.tooling)
  implementation(libs.androidx.compose.ui.preview)
  testImplementation(libs.androidx.compose.ui.test)
}
