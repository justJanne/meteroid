@file:Suppress("UnstableApiUsage")

/*
 * Quasseldroid - Quassel client for Android
 *
 * Copyright (c) 2019 Janne Mareike Koschinski
 * Copyright (c) 2019 The Quassel Project
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

plugins {
  id("justjanne.android.library")
  alias(libs.plugins.kotlin.serialization)
  alias(libs.plugins.kotlin.ksp)
  alias(libs.plugins.dagger.hilt)
}

android {
  namespace = "de.chaosdorf.meteroid.persistence"
}

ksp {
  arg("room.generateKotlin", "true")
  arg("room.schemaLocation", "$projectDir/room/schemas")
}

dependencies {
  implementation(libs.kotlin.stdlib)
  implementation(libs.kotlinx.datetime)
  implementation(libs.kotlinx.serialization.json)
  coreLibraryDesugaring(libs.desugar.jdk)

  implementation(libs.kotlinx.coroutines.core)
  testImplementation(libs.kotlinx.coroutines.test)

  testImplementation(libs.kotlin.test)
  testImplementation(libs.junit.api)
  testImplementation(libs.junit.params)
  testRuntimeOnly(libs.junit.engine)

  implementation(libs.androidx.room.runtime)
  ksp(libs.androidx.room.compiler)
  implementation(libs.androidx.room.ktx)
  implementation(libs.androidx.room.paging)

  implementation(libs.hilt.android)
  ksp(libs.hilt.compiler)

  implementation(project(":api"))
}
