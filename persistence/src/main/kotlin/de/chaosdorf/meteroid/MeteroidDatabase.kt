/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import de.chaosdorf.meteroid.model.*
import de.chaosdorf.meteroid.util.BigDecimalTypeConverter
import de.chaosdorf.meteroid.util.KotlinDatetimeTypeConverter

@Database(
  version = 1,
  entities = [
    Drink::class,
    Server::class,
    User::class,
    PinnedUser::class,
    Transaction::class
  ],
  autoMigrations = [],
)
@TypeConverters(
  value = [
    KotlinDatetimeTypeConverter::class,
    BigDecimalTypeConverter::class
  ]
)
abstract class MeteroidDatabase : RoomDatabase() {
  abstract fun drinks(): DrinkRepository
  abstract fun server(): ServerRepository
  abstract fun users(): UserRepository
  abstract fun pinnedUsers(): PinnedUserRepository
  abstract fun transactions(): TransactionRepository
}
