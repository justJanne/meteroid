/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.model

import androidx.room.*
import de.chaosdorf.mete.model.ServerId
import kotlinx.coroutines.flow.Flow

@Entity
data class Server(
  @PrimaryKey
  val serverId: ServerId,
  val name: String?,
  val url: String,
  val logoUrl: String?
)

@Dao
interface ServerRepository {
  @Query("SELECT * FROM Server WHERE serverId = :id LIMIT 1")
  suspend fun get(id: ServerId): Server?

  @Query("SELECT * FROM Server WHERE serverId = :id LIMIT 1")
  fun getFlow(id: ServerId): Flow<Server?>

  @Query("SELECT count(*) FROM Server")
  fun countFlow(): Flow<Int>

  @Query("SELECT * FROM Server")
  suspend fun getAll(): List<Server>

  @Query("SELECT * FROM Server")
  fun getAllFlow(): Flow<List<Server>>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun save(server: Server)

  @Query("DELETE FROM Server WHERE serverId = :id")
  suspend fun delete(id: ServerId)

  @Query("DELETE FROM Server")
  suspend fun deleteAll()
}
