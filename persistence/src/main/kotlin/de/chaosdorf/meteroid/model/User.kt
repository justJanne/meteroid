/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.model

import androidx.room.*
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import de.chaosdorf.mete.model.UserModel
import de.chaosdorf.meteroid.util.gravatarUrl
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal

@Entity(
  primaryKeys = ["serverId", "userId"],
  foreignKeys = [
    ForeignKey(Server::class, ["serverId"], ["serverId"])
  ]
)
data class User(
  val serverId: ServerId,
  val userId: UserId,
  val active: Boolean,
  val name: String,
  val email: String?,
  val balance: BigDecimal,
  val audit: Boolean,
  val redirect: Boolean,
) {
  val gravatarUrl: String? by lazy { email?.let(::gravatarUrl) }

  companion object {
    fun fromModel(server: Server, value: UserModel) = User(
      server.serverId,
      value.userId,
      value.active,
      value.name,
      value.email,
      value.balance,
      value.audit,
      value.redirect,
    )
  }
}

@Dao
interface UserRepository {
  @Query("SELECT * FROM User WHERE serverId = :serverId AND userId = :userId LIMIT 1")
  suspend fun get(serverId: ServerId, userId: UserId): User?

  @Query("SELECT * FROM User WHERE serverId = :serverId AND userId = :userId LIMIT 1")
  fun getFlow(serverId: ServerId, userId: UserId): Flow<User?>

  @Query("SELECT * FROM User ORDER BY NAME ASC")
  suspend fun getAll(): List<User>

  @Query("SELECT * FROM User ORDER BY NAME ASC")
  fun getAllFlow(): Flow<List<User>>

  @Query("SELECT * FROM User WHERE serverId = :serverId ORDER BY NAME ASC")
  suspend fun getAll(serverId: ServerId): List<User>

  @Query("SELECT * FROM User WHERE serverId = :serverId ORDER BY NAME ASC")
  fun getAllFlow(serverId: ServerId): Flow<List<User>>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun save(user: User)

  @Query("DELETE FROM User WHERE serverId = :serverId AND userId = :userId")
  suspend fun delete(serverId: ServerId, userId: UserId)

  @Query("DELETE FROM User WHERE serverId = :serverId")
  suspend fun deleteAll(serverId: ServerId)
}
