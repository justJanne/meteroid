/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.model

import androidx.room.*
import de.chaosdorf.mete.model.*
import kotlinx.coroutines.flow.Flow
import kotlinx.datetime.Instant
import java.math.BigDecimal

@Entity(
  primaryKeys = ["serverId", "transactionId"],
  foreignKeys = [
    ForeignKey(Server::class, ["serverId"], ["serverId"]),
    ForeignKey(
      User::class,
      ["serverId", "userId"],
      ["serverId", "userId"]
    ),
    ForeignKey(
      Drink::class,
      ["serverId", "drinkId"],
      ["serverId", "drinkId"]
    )
  ],
  indices = [
    Index("serverId", "userId"),
    Index("serverId", "drinkId")
  ]
)
data class Transaction(
  val serverId: ServerId,
  val transactionId: TransactionId,
  val userId: UserId,
  val drinkId: DrinkId?,
  val difference: BigDecimal,
  val timestamp: Instant
) {
  companion object {
    fun fromModel(server: Server, userId: UserId, value: TransactionModel) = Transaction(
      server.serverId,
      value.transactionId,
      userId,
      value.drinkId,
      value.difference,
      value.timestamp
    )
  }
}

@Dao
interface TransactionRepository {
  @Query("SELECT * FROM `Transaction` WHERE serverId = :serverId AND userId = :userId ORDER BY timestamp DESC")
  suspend fun getAll(serverId: ServerId, userId: UserId): List<Transaction>

  @Query("SELECT * FROM `Transaction` WHERE serverId = :serverId AND userId = :userId ORDER BY timestamp DESC")
  fun getAllFlow(serverId: ServerId, userId: UserId): Flow<List<Transaction>>

  @Query("SELECT * FROM `Transaction` WHERE serverId = :serverId AND userId = :userId ORDER BY timestamp DESC LIMIT 1")
  suspend fun getLatest(serverId: ServerId, userId: UserId): Transaction?

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun save(transaction: Transaction)

  @Query("DELETE FROM `Transaction` WHERE serverId = :serverId AND transactionId = :transactionId")
  suspend fun delete(serverId: ServerId, transactionId: TransactionId)

  @Query("DELETE FROM `Transaction` WHERE serverId = :serverId AND userId = :userId")
  suspend fun deleteAll(serverId: ServerId, userId: UserId)

  @Query("DELETE FROM `Transaction` WHERE serverId = :serverId")
  suspend fun deleteAll(serverId: ServerId)
}
