/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.model

import androidx.room.*
import de.chaosdorf.mete.model.ServerId
import de.chaosdorf.mete.model.UserId
import kotlinx.coroutines.flow.Flow

@Entity(
  primaryKeys = ["serverId", "userId"],
  foreignKeys = [
    ForeignKey(Server::class, ["serverId"], ["serverId"]),
    ForeignKey(User::class, ["serverId", "userId"], ["serverId", "userId"])
  ]
)
data class PinnedUser(
  val serverId: ServerId,
  val userId: UserId
)

@Dao
interface PinnedUserRepository {
  @Query("SELECT 1 FROM PinnedUser WHERE serverId = :serverId AND userId = :userId LIMIT 1")
  suspend fun isPinned(serverId: ServerId, userId: UserId): Boolean

  @Query("SELECT 1 FROM PinnedUser WHERE serverId = :serverId AND userId = :userId LIMIT 1")
  fun isPinnedFlow(serverId: ServerId, userId: UserId): Flow<Boolean>

  @Query("SELECT userId FROM PinnedUser WHERE serverId = :serverId")
  suspend fun getAll(serverId: ServerId): List<UserId>

  @Query("SELECT userId FROM PinnedUser WHERE serverId = :serverId")
  fun getAllFlow(serverId: ServerId): Flow<List<UserId>>

  @Query("SELECT User.* FROM PinnedUser JOIN User on PinnedUser.userId = User.userId AND PinnedUser.serverId = User.serverId")
  fun getPinnedUsersFlow(): Flow<List<User>>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun save(user: PinnedUser)

  @Query("DELETE FROM PinnedUser WHERE serverId = :serverId AND userId = :userId")
  suspend fun delete(serverId: ServerId, userId: UserId)

  @Query("DELETE FROM PinnedUser WHERE serverId = :serverId")
  suspend fun deleteAll(serverId: ServerId)
}
