/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.meteroid.model

import androidx.room.*
import de.chaosdorf.mete.model.DrinkId
import de.chaosdorf.mete.model.DrinkModel
import de.chaosdorf.mete.model.ServerId
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal
import java.net.URI

@Entity(
  primaryKeys = ["serverId", "drinkId"],
  foreignKeys = [
    ForeignKey(Server::class, ["serverId"], ["serverId"])
  ]
)
data class Drink(
  val serverId: ServerId,
  val drinkId: DrinkId,
  val active: Boolean,
  val name: String,
  val volume: BigDecimal,
  val caffeine: Int?,
  val price: BigDecimal,
  val logoUrl: String?,
) {
  val originalLogoUrl
    get() = logoUrl?.replace("/thumb/", "/original/")

  companion object {
    fun fromModel(server: Server, value: DrinkModel) = Drink(
      server.serverId,
      value.drinkId,
      value.active,
      value.name,
      value.volume,
      value.caffeine,
      value.price,
      value.logoUrl?.let {
        URI.create(server.url).resolve(it).toString()
      }
    )
  }
}

@Dao
interface DrinkRepository {
  @Query("SELECT * FROM Drink WHERE serverId = :serverId AND drinkId = :drinkId LIMIT 1")
  suspend fun get(serverId: ServerId, drinkId: DrinkId): Drink?

  @Query("SELECT * FROM Drink WHERE serverId = :serverId AND drinkId = :drinkId LIMIT 1")
  fun getFlow(serverId: ServerId, drinkId: DrinkId): Flow<Drink?>

  @Query("SELECT * FROM Drink WHERE serverId = :serverId ORDER BY NAME ASC")
  suspend fun getAll(serverId: ServerId): List<Drink>

  @Query("SELECT * FROM Drink WHERE serverId = :serverId ORDER BY ACTIVE DESC, NAME ASC")
  fun getAllFlow(serverId: ServerId): Flow<List<Drink>>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun save(drink: Drink)

  @Query("DELETE FROM Drink WHERE serverId = :serverId AND drinkId = :drinkId")
  suspend fun delete(serverId: ServerId, drinkId: DrinkId)

  @Query("DELETE FROM Drink WHERE serverId = :serverId")
  suspend fun deleteAll(serverId: ServerId)
}
