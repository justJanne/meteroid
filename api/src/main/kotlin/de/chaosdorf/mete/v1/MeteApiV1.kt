/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.mete.v1

import de.chaosdorf.mete.model.*
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.math.BigDecimal

internal interface MeteApiV1 : MeteApi {
  @GET("manifest.json")
  override suspend fun getManifest(): PwaManifest?

  @GET("api/v1/audits.json")
  override suspend fun listTransactions(
    @Query("user") user: UserId,
    @Query("start_date[year]") startYear: Int,
    @Query("start_date[month]") startMonth: Int,
    @Query("start_date[day]") startDay: Int,
    @Query("end_date[year]") endYear: Int,
    @Query("end_date[month]") endMonth: Int,
    @Query("end_date[day]") endDay: Int,
  ): TransactionSummaryModelV1

  @GET("api/v1/barcodes.json")
  override suspend fun listBarcodes(): List<BarcodeModelV1>

  @GET("api/v1/barcodes/{id}.json")
  override suspend fun getBarcode(
    @Path("id") id: BarcodeId
  ): BarcodeModelV1?

  @GET("api/v1/drinks.json")
  override suspend fun listDrinks(): List<DrinkModelV1>

  @GET("api/v1/drinks/{id}.json")
  override suspend fun getDrink(
    @Path("id") id: DrinkId
  ): DrinkModelV1?

  @GET("api/v1/users.json")
  override suspend fun listUsers(): List<UserModelV1>

  @GET("api/v1/users/{id}.json")
  override suspend fun getUser(
    @Path("id") id: UserId
  ): UserModelV1?

  @GET("api/v1/users/{id}/deposit.json")
  override suspend fun deposit(
    @Path("id") id: UserId,
    @Query("amount") amount: BigDecimal
  )

  @GET("api/v1/users/{id}/payment.json")
  override suspend fun withdraw(
    @Path("id") id: UserId,
    @Query("amount") amount: BigDecimal
  )

  @GET("api/v1/users/{id}/buy.json")
  override suspend fun purchase(
    @Path("id") id: UserId,
    @Query("drink") drink: DrinkId
  )

  @GET("api/v1/users/{id}/buy_barcode.json")
  override suspend fun purchase(
    @Path("id") id: UserId,
    @Query("barcode") barcode: BarcodeId
  )
}
