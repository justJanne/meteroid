/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2013-2023 Chaosdorf e.V.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.chaosdorf.mete.model

import java.math.BigDecimal

interface MeteApi {
  suspend fun getManifest(): PwaManifest?
  suspend fun listTransactions(
    user: UserId,
    startYear: Int, startMonth: Int, startDay: Int,
    endYear: Int, endMonth: Int, endDay: Int,
  ): TransactionSummaryModel

  suspend fun listBarcodes(): List<BarcodeModel>
  suspend fun getBarcode(id: BarcodeId): BarcodeModel?
  suspend fun listDrinks(): List<DrinkModel>
  suspend fun getDrink(id: DrinkId): DrinkModel?
  suspend fun listUsers(): List<UserModel>
  suspend fun getUser(id: UserId): UserModel?
  suspend fun deposit(id: UserId, amount: BigDecimal)
  suspend fun withdraw(id: UserId, amount: BigDecimal)
  suspend fun purchase(id: UserId, drink: DrinkId)
  suspend fun purchase(id: UserId, barcode: BarcodeId)
}
